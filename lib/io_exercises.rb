def guessing_game
  guesses = []
  result = rand(1..100)
  number = 0
  until number == result
    puts "guess a number"
    number = gets.chomp.to_i
      puts "#{number}"
      if number < result
        puts "too low"
      else number > result
        puts "too high"
      end
    guesses << number
  end
  puts guesses.size
end

def name_file
  puts "Please enter a file name."
  file = gets.chomp
  shuffled = File.readlines(file).shuffle
  new_file = File.open("#{file}-shuffled.txt", "w")
  new_file.puts shuffled
end

name_file

# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
